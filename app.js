const express = require('express')
const app = express()
const cors = require('cors')
const helmet = require('helmet')
const morgan = require('morgan')
const compression = require("compression");
const mongoose = require('mongoose')
require ('dotenv/config')
const articlesRoute = require('./routes/articlesRoute')

//environment variables
const port = process.env.PORT
const mongoPort = process.env.DB_CONNECTION

// middlewares
app.use(cors({
    origin: ["http://localhost:4200"],
    methods: ["GET", "POST", "PUT", "DELETE"], 
    allowedHeaders: ["Content-Type", "Authorization"]}))
app.use(express.json())
app.use(helmet())
app.use(morgan("common"))
app.use(compression());


//routes
app.get('/', (req, res) => res.status(200).send('Hello World!'))
app.use('/articles', articlesRoute);


//mongo connection
const mongoConnection = async () => {
    try {
      await mongoose.connect(
        mongoPort,
        { useCreateIndex: true, useNewUrlParser: true, useUnifiedTopology: true}
      );
      console.log('Connection to mongoDB Successful');
    } catch (err) {
      console.log('Connection to mongoDB Failed');
    }
  };
mongoConnection()


//server start
app.listen(port, () => console.log(`app listening on port ${port}`))
