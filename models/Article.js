const mongoose = require('mongoose')

const ArticleSchema = mongoose.Schema({
    name: String,
    price: Number,
    categories: [String]
});


module.exports = mongoose.model('Article', ArticleSchema);