const express = require('express');
const router = express.Router();
const Article = require('../models/Article');

//get all the articles
router.get('/', async (req, res) => {
    try {
        const articles = await Article.find();
        res.status(200).json(articles);
    } catch (error) {
         res.json({message: error});
    }
});


module.exports = router;